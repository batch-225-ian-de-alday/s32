let http = require('http');


server = http.createServer((request,response) => {
   
    if (request.url == "/" && request.method == "GET") {

        response.writeHead(200, {'Content-type' : 'text/plain'})
        response.write("Welcome to booking system");
        response.end();        
    }

    if (request.url == "/profile" && request.method == "GET") {

        response.writeHead(200, {'Content-type' : 'text/plain'})
        response.write("Welcome to your profile!");
        response.end();        
    }

    if (request.url == "/courses" && request.method == "GET") {

        response.writeHead(200, {'Content-type' : 'text/plain'})
        response.write("Here’s our courses available");
        response.end();        
    }

    if (request.url == "/addcourses" && request.method == "POST") {

        response.writeHead(200, {'Content-type' : 'text/plain'})
        response.write("Add a course to our resources");
        response.end();        
    }

    if (request.url == "/updatecourses" && request.method == "PUT") {

        response.writeHead(200, {'Content-type' : 'text/plain'})
        response.write("Update a course to our resources");
        response.end();        
    }

    if (request.url == "/archivecourses" && request.method == "DELETE") {

        response.writeHead(200, {'Content-type' : 'text/plain'})
        response.write("Archive courses to our resources");
        response.end();   
           
    } 
   
}).listen(4000);

console.log("Server is currently running at localhost:4000");
   
    


